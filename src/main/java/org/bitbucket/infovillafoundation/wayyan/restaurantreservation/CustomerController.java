package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Admin on 2/12/2014.
 */

@FXMLController(value = "/fxml/customer.fxml", title = "Customer Details")
public class CustomerController {
    @FXML
    private TextField name;

    @FXML
    private TextField id;

    @FXML
    private TextField phonenumber;

    @FXML
    private TextField bringing;

    @FXML
    @ActionTrigger("notedetails")
    private Button notedetails;

    @FXML
    @BackAction
    private Button back;

    @FXML
    private Text noted;

    @Inject
    private Restaurant restaurant;

    @ActionMethod("notedetails")
    public void notedetails() {
        noted.setText("Your details have been noted!");
        String customerName = name.getText();
        String customerID = id.getText();
        String customerPhoneNumber = phonenumber.getText();
        int customerBringing = Integer.parseInt(bringing.getText());
        Customer customer = new Customer(customerName, customerID, customerPhoneNumber, customerBringing);
        restaurant.addCustomer(customer);
    }
}
