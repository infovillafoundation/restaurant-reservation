package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import org.datafx.controller.flow.injection.FlowScoped;

/**
 * Created by Admin on 3/12/2014.
 */

@FlowScoped
public class TableHolder {
    private Table table;

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
