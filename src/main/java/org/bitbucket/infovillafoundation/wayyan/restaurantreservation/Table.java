package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

/**
 * Created by Admin on 2/12/2014.
 */
public class Table {
    private int tableNumber;
    private String shape;
    private int seats;
    private String status;
    private boolean available;
    private Customer customer;

    public Table(int tableNumber, String shape, int seats, String status, boolean available) {
        this.tableNumber = tableNumber;
        this.shape = shape;
        this.seats = seats;
        this.status = status;
        this.available = available;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Table{" +
                "tableNumber=" + tableNumber +
                ", shape='" + shape + '\'' +
                ", seats=" + seats +
                ", status='" + status + '\'' +
                ", available=" + available +
                ", customer=" + customer +
                '}';
    }
}
