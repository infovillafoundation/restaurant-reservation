package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 3/12/2014.
 */

@FXMLController(value = "/fxml/bookingtable.fxml", title = "Table Booking")
public class BookingTableController {
    @Inject
    private Restaurant restaurant;

    @Inject
    private TableHolder tableHolder;

    @FXML
    private TableView<Table> tablesToBook;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @LinkAction(BookingCustomerController.class)
    private Button choose;

    @PostConstruct
    public void init() {
        tablesToBook.getItems().setAll(restaurant.getTables());
        tablesToBook.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Table>() {
            @Override
            public void changed(ObservableValue<? extends Table> observable, Table oldValue, Table newValue) {
                tableHolder.setTable(newValue);
                if (newValue.getCustomer() != null)
                    choose.setVisible(false);
                else
                    choose.setVisible(true);
            }
        });
    }
}
