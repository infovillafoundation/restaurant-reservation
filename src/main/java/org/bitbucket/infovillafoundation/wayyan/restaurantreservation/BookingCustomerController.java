package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 3/12/2014.
 */

@FXMLController(value = "/fxml/bookingcustomer.fxml", title = "Customer Bookings")
public class BookingCustomerController {
    @Inject
    private Restaurant restaurant;

    @Inject
    private TableHolder tableHolder;

    private Customer selectedCustomer;

    @FXML
    private TableView<Customer> customersForBooking;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("assignAndQuit")
    private Button assign;

    @PostConstruct
    public void init() {
        customersForBooking.getItems().setAll(restaurant.getFreeCustomers());
        customersForBooking.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Customer>() {
            @Override
            public void changed(ObservableValue<? extends Customer> observable, Customer oldValue, Customer newValue) {
                selectedCustomer = newValue;
            }
        });
    }

    @ActionMethod("assignCustomer")
    public void assignCustomer() {
        restaurant.assignTable(tableHolder.getTable(), selectedCustomer);
    }
}
