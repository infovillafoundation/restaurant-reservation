package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Admin on 2/12/2014.
 */

@FXMLController(value = "/fxml/restaurant.fxml", title = "Restaurant Reservation")
public class RestaurantController {
    @FXML
    private TextField tablenumber;

    @FXML
    private TextField shape;

    @FXML
    private TextField seats;

    @FXML
    private TextField status;

    @FXML
    @ActionTrigger("booktables")
    private Button booktable;

    @FXML
    private Text booked;

    private List<Table> tables;

    @PostConstruct
    public void init() {
        tables = new ArrayList<>();
    }

    @ActionMethod("booktables")
    public void booktables() {
        booked.setText("You have booked the table successfully!");
        int tableNo = Integer.parseInt(tablenumber.getText());
        String tableShape = shape.getText();
        int tableSeats = Integer.parseInt(seats.getText());
        String tableStatus = status.getText();
        Table table = new Table(tableNo, tableShape, tableSeats, tableStatus, true);
        tables.add(table);
        tablenumber.setText("");
        shape.setText("");
        seats.setText("");
        status.setText("");

        System.out.println(tables);
    }
}
