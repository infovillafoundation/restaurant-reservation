package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import org.datafx.controller.injection.ApplicationScoped;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/12/2014.
 */

@ApplicationScoped
public class Restaurant {
    private List<Table> tables;
    private List<Customer> customers;
    private List<Customer> freeCustomers;

    public Restaurant() {
        tables = new ArrayList<>();
        customers = new ArrayList<>();
        freeCustomers = new ArrayList<>();
    }

    public void addTable(Table table) {
        tables.add(table);
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
        freeCustomers.add(customer);
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void assignTable(Table table, Customer customer) {
        table.setCustomer(customer);
        freeCustomers.remove(customer);
    }

    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    public List<Customer> getFreeCustomers() {
        return freeCustomers;
    }

    public void setFreeCustomers(List<Customer> freeCustomers) {
        this.freeCustomers = freeCustomers;
    }
}
