package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Admin on 2/12/2014.
 */

@FXMLController(value = "/fxml/table.fxml", title = "Table Reservation")
public class TableController {
    @FXML
    private TextField tablenumber;

    @FXML
    private ComboBox<String> shape;

    @FXML
    private TextField seats;

    @FXML
    private ComboBox<String> status;

    @FXML
    @ActionTrigger("booktables")
    private Button booktable;

    @FXML
    @BackAction
    private Button back;

    @FXML
    private Text booked;

    @Inject
    private Restaurant restaurant;

    @PostConstruct
    public void init() {
        status.setValue(status.getItems().get(0));
        shape.setValue(shape.getItems().get(0));
    }

    @ActionMethod("booktables")
    public void booktables() {
        booked.setText("You have booked the table successfully!");
        int tableNo = Integer.parseInt(tablenumber.getText());
        String tableShape = shape.getValue();
        int tableSeats = Integer.parseInt(seats.getText());
        String tableStatus = status.getValue();
        Table table = new Table(tableNo, tableShape, tableSeats, tableStatus, true);
        restaurant.addTable(table);
        tablenumber.setText("");
        shape.setValue(shape.getItems().get(0));
        seats.setText("");
        status.setValue(status.getItems().get(0));

        System.out.println(restaurant.getTables());
    }
}
