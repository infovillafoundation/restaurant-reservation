package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

/**
 * Created by Admin on 2/12/2014.
 */
public class Customer {

    String name;
    String id;
    String phonenumber;
    int bringingGuests;

    public Customer(String name, String id, String phonenumber, int bringingGuests) {
        this.name = name;
        this.id = id;
        this.phonenumber = phonenumber;
        this.bringingGuests = bringingGuests;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getBringingGuests() {
        return bringingGuests;
    }

    public void setBringingGuests(int bringingGuests) {
        this.bringingGuests = bringingGuests;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", bringingGuests=" + bringingGuests +
                '}';
    }
}


