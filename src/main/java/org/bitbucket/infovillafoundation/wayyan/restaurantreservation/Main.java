package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.application.Application;
import javafx.stage.Stage;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.action.FlowActionChain;
import org.datafx.controller.flow.action.FlowLink;
import org.datafx.controller.flow.action.FlowMethodAction;

/**
 * Created by Admin on 2/12/2014.
 */
public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        new Flow(MenuController.class)
                .withAction(BookingCustomerController.class, "assignAndQuit", new FlowActionChain(new FlowMethodAction("assignCustomer"), new FlowLink<>(MenuController.class)))
                .startInStage(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
