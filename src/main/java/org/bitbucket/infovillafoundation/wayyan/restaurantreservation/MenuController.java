package org.bitbucket.infovillafoundation.wayyan.restaurantreservation;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.LinkAction;

/**
 * Created by Admin on 2/12/2014.
 */

@FXMLController(value = "/fxml/menu.fxml", title = "Menu Details")
public class MenuController {
    @FXML
    @LinkAction(TableController.class)
    private Button tablenav;

    @FXML
    @LinkAction(CustomerController.class)
    private Button customernav;

    @FXML
    @LinkAction(BookingTableController.class)
    private Button bookingnav;
}
